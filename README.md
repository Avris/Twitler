## Twitler

A Chrome extension that replaces "Donald Trump" on all websites with "Twitler".

### Copyright

* **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
