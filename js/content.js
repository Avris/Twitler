const replace = () => {
    const walker = document.createTreeWalker(
        document.body,
        NodeFilter.SHOW_TEXT,
        null,
        false
    );

    let node;
    while (node = walker.nextNode()) {
        node.nodeValue = node.nodeValue.replace(/(donald )?trump/ig, 'Twitler');
    }
};

replace();
setTimeout(replace, 1000);
setTimeout(replace, 2000);
